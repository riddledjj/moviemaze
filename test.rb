def multiplication_table(size)
  puts "Таблица умножения #{size}x#{size}:"
  puts "------------------------"

  for i in 1..size
    for j in 1..size
       print "#{i * j}\t"
    end
    # puts "\n"
  end
end

puts "Введите размер таблицы умножения: "
table_size = gets.chomp.to_i

multiplication_table(table_size)
