class MoviesController < ApplicationController
 def index
  @movies = Movie.all
 end

 def show
  @movie = Movie.find(params[:id])
 end

 def new
  @movie = Movie.new
  @movie.build_director
  @movie.actors.build
 end

 def create
  @movie = Movie.new(movie_params)
  if @movie.save
   redirect_to @movie, notice: 'Movie was successfully created.'
  else
   render :new
  end
 end
 private

 def movie_params
  params.require(:movie).permit(:title, :year, :description, :director_id, actor_ids: [], actors_attributes: [:id, :fullname, :_destroy], director_attributes: [:id, :fullname])
 end



end
