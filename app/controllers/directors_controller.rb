class DirectorsController < ApplicationController

   def index
  @directors = Director.all
 end

  def new
    @director = Director.new
  end
  def show
  @director = Director.find(params[:id])

  end
  def create
    @director = Director.new(director_params)

    if @director.save
      redirect_to @director, notice: 'Режиссер успешно создан.'
    else
      render :new
    end
  end

  def edit
    @director = Director.find(params[:id])
  end

  def update
    @director = Director.find(params[:id])

    if @director.update(director_params)
      redirect_to @director, notice: 'Режиссер успешно обновлен.'
    else
      render :edit
    end
  end

  def destroy
    @director = Director.find(params[:id])
    @director.destroy

    respond_to do |format|
      format.html { redirect_to directors_url, notice: 'Director was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  private

  def director_params
    params.require(:director).permit(:fullname)
  end
end
