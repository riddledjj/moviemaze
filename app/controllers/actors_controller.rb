class ActorsController < ApplicationController
 def index
  @actors = Actor.all
 end

 def new
  @actor = Actor.new
 end

 def create
  @actor = Actor.new(actor_params)

  if @actor.save
   redirect_to @actor, notice: 'Актер успешно создан.'
  else
   render :new
  end
 end

 def show
  @actor = Actor.find(params[:id])
 end

 private

 def actor_params
  params.require(:actor).permit(:fullname)
 end

end
