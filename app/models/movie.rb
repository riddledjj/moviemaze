class Movie < ApplicationRecord
 has_many :actor_movies
 has_many :actors, through: :actor_movies
 has_one :director_movie
 has_one :director, through: :director_movie

 accepts_nested_attributes_for :director, :actors
 validates :title, presence: true
end
