Rails.application.routes.draw do
  get 'pages/contact'
   root "movies#index"
   resources :movies
   resources :actors
   resources :directors
end
