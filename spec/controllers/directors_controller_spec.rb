require 'rails_helper'

RSpec.describe DirectorsController, type: :controller do
  describe 'GET #index' do
    it 'возвращает успешный HTTP статус' do
      get :index
      expect(response).to have_http_status(:success)
    end

    it 'назначает все режиссеры как @directors' do
      directors = FactoryBot.create_list(:director, 3)
      get :index
      expect(assigns(:directors)).to eq(directors)
    end
  end

  describe 'GET #show' do
    it 'возвращает успешный HTTP статус' do
      director = FactoryBot.create(:director)
      get :show, params: { id: director.id }
      expect(response).to have_http_status(:success)
    end

    it 'назначает запрошенного режиссера как @director' do
      director = FactoryBot.create(:director)
      get :show, params: { id: director.id }
      expect(assigns(:director)).to eq(director)
    end
  end

  describe 'GET #new' do
    it 'возвращает успешный HTTP статус' do
      get :new
      expect(response).to have_http_status(:success)
    end

    it 'назначает новый экземпляр режиссера как @director' do
      get :new
      expect(assigns(:director)).to be_a_new(Director)
    end
  end

  describe 'POST #create' do
    context 'с допустимыми параметрами' do
      it 'создает нового режиссера' do
        expect {
          post :create, params: { director: FactoryBot.attributes_for(:director) }
        }.to change(Director, :count).by(1)
      end

      it 'перенаправляет на нового режиссера' do
        post :create, params: { director: FactoryBot.attributes_for(:director) }
        expect(response).to redirect_to(Director.last)
      end
    end

    context 'с недопустимыми параметрами' do
      it 'не создает нового режиссера' do
        expect {
          post :create, params: { director: FactoryBot.attributes_for(:director, fullname: nil) }
        }.not_to change(Director, :count)
      end

      it 'рендерит новый шаблон' do
        post :create, params: { director: FactoryBot.attributes_for(:director, fullname: nil) }
        expect(response).to render_template('new')
      end
    end
  end

  describe 'GET #edit' do
    it 'возвращает успешный HTTP статус' do
      director = FactoryBot.create(:director)
      get :edit, params: { id: director.id }
      expect(response).to have_http_status(:success)
    end

    it 'назначает запрошенного режиссера как @director' do
      director = FactoryBot.create(:director)
      get :edit, params: { id: director.id }
      expect(assigns(:director)).to eq(director)
    end
  end

  describe 'PUT #update' do
    context 'с допустимыми параметрами' do
      let(:director) { FactoryBot.create(:director) }

      it 'обновляет режиссера' do
        put :update, params: { id: director.id, director: { fullname: 'Updated Name' } }
        director.reload
        expect(director.fullname).to eq('Updated Name')
        expect(response).to redirect_to(director)
      end
    end

    context 'с недопустимыми параметрами' do
      let(:director) { FactoryBot.create(:director) }

      it 'не обновляет режиссера' do
        put :update, params: { id: director.id, director: { fullname: nil } }
        director.reload
        expect(director.fullname).not_to be_nil
        expect(response).to render_template('edit')
      end
    end
  end


  describe "DELETE #destroy" do
    let!(:director) { FactoryBot.create(:director) }

    it "destroys the director" do
      expect {
        delete :destroy, params: { id: director.id }
      }.to change(Director, :count).by(-1)

      expect(response).to redirect_to(directors_url)
    end
  end
end
