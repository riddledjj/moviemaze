require 'rails_helper'

RSpec.describe ActorsController, type: :controller do
  describe 'GET #index' do
    it 'assigns all actors to @actors' do
      actor = FactoryBot.create(:actor)
      get :index
      expect(assigns(:actors)).to eq([actor])
    end

    it 'renders the index template' do
      get :index
      expect(response).to render_template('index')
    end
  end

  describe 'GET #new' do
    it 'assigns a new actor to @actor' do
      get :new
      expect(assigns(:actor)).to be_a_new(Actor)
    end

    it 'renders the new template' do
      get :new
      expect(response).to render_template('new')
    end
  end

  describe 'POST #create' do
    context 'with valid parameters' do
      it 'creates a new actor' do
        expect {
          post :create, params: { actor: FactoryBot.attributes_for(:actor) }
        }.to change(Actor, :count).by(1)
      end

      it 'redirects to the created actor' do
        post :create, params: { actor: FactoryBot.attributes_for(:actor) }
        expect(response).to redirect_to(Actor.last)
      end

      it 'sets a flash notice' do
        post :create, params: { actor: FactoryBot.attributes_for(:actor) }
        expect(flash[:notice]).to be_present
      end
    end

    context 'with invalid parameters' do
      it 'does not create a new actor' do
        expect {
          post :create, params: { actor: FactoryBot.attributes_for(:actor, fullname: nil) }
        }.to_not change(Actor, :count)
      end

      it 'renders the new template' do
        post :create, params: { actor: FactoryBot.attributes_for(:actor, fullname: nil) }
        expect(response).to render_template('new')
      end
    end
  end

  describe 'GET #show' do
    it 'assigns the requested actor to @actor' do
      actor = FactoryBot.create(:actor)
      get :show, params: { id: actor.id }
      expect(assigns(:actor)).to eq(actor)
    end

    it 'renders the show template' do
      actor = FactoryBot.create(:actor)
      get :show, params: { id: actor.id }
      expect(response).to render_template('show')
    end
  end
end
