# spec/controllers/movies_controller_spec.rb

require 'rails_helper'

RSpec.describe MoviesController, type: :controller do
  describe 'GET #index' do
    it 'assigns all movies to @movies' do
      movie = FactoryBot.create(:movie)
      get :index
      expect(assigns(:movies)).to eq([movie])
    end

    it 'renders the index template' do
      get :index
      expect(response).to render_template('index')
    end
  end

  describe 'GET #show' do
    it 'assigns the requested movie to @movie' do
      movie = FactoryBot.create(:movie)
      get :show, params: { id: movie.id }
      expect(assigns(:movie)).to eq(movie)
    end

    it 'renders the show template' do
      movie = FactoryBot.create(:movie)
      get :show, params: { id: movie.id }
      expect(response).to render_template('show')
    end
  end

  describe 'GET #new' do
    it 'assigns a new movie to @movie' do
      get :new
      expect(assigns(:movie)).to be_a_new(Movie)
    end

    it 'builds a new director for @movie' do
      get :new
      expect(assigns(:movie).director).to be_a_new(Director)
    end

    it 'builds a new actor for @movie' do
      get :new
      expect(assigns(:movie).actors.first).to be_a_new(Actor)
    end

    it 'renders the new template' do
      get :new
      expect(response).to render_template('new')
    end
  end

  describe 'POST #create' do
    context 'with valid parameters' do
      it 'creates a new movie' do
        expect {
          post :create, params: { movie: FactoryBot.attributes_for(:movie) }
        }.to change(Movie, :count).by(1)
      end

      it 'redirects to the created movie' do
        post :create, params: { movie: FactoryBot.attributes_for(:movie) }
        expect(response).to redirect_to(Movie.last)
      end
    end

    context 'with invalid parameters' do
      it 'does not create a new movie' do
        expect {
          post :create, params: { movie: FactoryBot.attributes_for(:movie, title: nil) }
        }.to_not change(Movie, :count)
      end

      it 'renders the new template' do
        post :create, params: { movie: FactoryBot.attributes_for(:movie, title: nil) }
        expect(response).to render_template('new')
      end
    end
  end
end
