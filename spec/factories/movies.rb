# spec/factories/movies.rb

FactoryBot.define do
  factory :movie do
    title { Faker::Movie.title }
    year { Faker::Number.between(from: 1900, to: 2024) }
    description { Faker::Lorem.paragraph }

    # Association with Director
    director

    # Associations with Actors
    transient do
      actors_count { 3 } # You can adjust the number of actors as needed
    end

    after(:create) do |movie, evaluator|
      create_list(:actor, evaluator.actors_count, movies: [movie])
    end
  end
end

# spec/factories/directors.rb

FactoryBot.define do
  factory :director do
    fullname { Faker::Name.name }
  end
end

# spec/factories/actors.rb

FactoryBot.define do
  factory :actor do
    fullname { Faker::Name.name }
  end
end
