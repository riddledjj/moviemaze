class CreateMovies < ActiveRecord::Migration[7.1]
  def change
    create_table :movies do |t|
      t.text :title
      t.integer :year
      t.text :description
      t.timestamps
    end
  end
end
