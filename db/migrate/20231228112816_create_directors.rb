class CreateDirectors < ActiveRecord::Migration[7.1]
  def change
    create_table :directors do |t|
      t.text :fullname
      t.timestamps
    end
  end
end
